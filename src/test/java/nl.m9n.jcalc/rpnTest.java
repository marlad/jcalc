/*******************************************************************************
 * Copyright (C) 2020, M. Klein
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

package nl.m9n.jcalc;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class rpnTest.
 *
 */
public class rpnTest
{
    /**
     * Default constructor for test class rpnTest
     */
    public rpnTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    // TODO: error_on_infix_without_spaces()
    // String infix = "12+3*4";

    // TODO: error_on_incorrect_formula()
    // infix = "1 + 2 * 3 +";

    @Test
    public void multiply_precedes_addition() {
        String infix = "12 + 3 * 4";
        rpn postfix = new rpn(infix);
        String answer = postfix.solve();
        assert answer.equals("24");
    }

    @Test
    public void equal_operators() {
        String infix = "1 - 2 + 3";
        rpn postfix = new rpn(infix);
        String answer = postfix.solve();
        assert answer.equals("2");
    }

    @Test
    public void floating_point_calculation() {
        String infix = "1 - 2.3 + 3";
        rpn postfix = new rpn(infix);
        String answer = postfix.solve();
        assert answer.equals("1.7");
    }
}
