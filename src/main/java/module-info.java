module nl.m9n.jcalc {
	requires javafx.base;
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	
	opens nl.m9n.jcalc to javafx.fxml;
	exports nl.m9n.jcalc;
}
