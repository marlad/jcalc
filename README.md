# jcalc

Calculator written in Java/JavaFX

Uses infix notation for both input and output  
For calculation it makes use of the [Shunting-yard algorithm](https://en.wikipedia.org/wiki/Edsger_W._Dijkstra) (invented by Edsger Dijkstra)  
to convert the infix notation to Postfix (also known as [reverse polish notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation))  

![Screenshot](/screenshot.png?raw=true)

# Setup

## Maven

```
apt install -y maven
mvn compile javafx:run
```

## Manual

* you can build from source using the Makefile (may need some path/version adjustments):  

Download OpenJDK from https://jdk.java.net  
Download JavaFX from https://gluonhq.com/products/javafx/  
Download Junit 4 from https://junit.org/junit4/  

Extract to /opt/java/  

Compile and run

```
make
make run
```

Tested with:

- OpenJDK 11.0.2
- JavaFX 11.0.2
- Junit 4.13
