JAVAC = /usr/bin/javac
JVM = /usr/bin/java

JAVA_TEST_FLAGS = -ea --add-opens jcalc/jcalc=junit,ALL-UNNAMED --add-modules=ALL-MODULE-PATH

MODULE = jcalc
MODULE_PATH = -p ./bin:/opt/java/junit/junit-4.13.jar:/opt/java/junit/hamcrest-core-1.3.jar:/opt/java/javafx-sdk-11.0.2/lib/javafx.base.jar:/opt/java/javafx-sdk-11.0.2/lib/javafx.controls.jar:/opt/java/javafx-sdk-11.0.2/lib/javafx.fxml.jar:/opt/java/javafx-sdk-11.0.2/lib/javafx.graphics.jar

MAINSRC = src/main/java/nl.m9n.jcalc

TESTSRC = src/test/java/nl.m9n.jcalc

RESOURCES = src/main/resources

TARGET = bin

MAIN = nl.m9n.jcalc/nl.m9n.jcalc.Main

MAINTEST = rpnTest

.SUFFIXES : .class .java

all: 
	mkdir -p bin/jcalc
	cp $(RESOURCES)/*.fxml $(TARGET)
	$(JAVAC) $(MODULE_PATH) -d $(TARGET) src/main/java/module-info.java $(MAINSRC)/*.java

test:
	$(JAVAC) $(MODULE_PATH) -d $(TARGET) src/main/java/module-info.java $(TESTSRC)/$(MAINTEST).java

clean:
	rm -rf $(TARGET)

run:
	$(JVM) $(MODULE_PATH) -m $(MAIN)

runtest: 
	$(JVM) $(JAVA_TEST_FLAGS) $(MODULE_PATH) org.junit.runner.JUnitCore $(MODULE).$(MAINTEST)

.PHONY: all test clean run run_test
