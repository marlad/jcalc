/*******************************************************************************
 * Copyright (C) 2020, M. Klein
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

package nl.m9n.jcalc;

import java.util.*;

/**
 * rpn class (Reverse Polish Notation)
 *
 */
public class rpn {

    private final ArrayList<String> output = new ArrayList<>();
    private final Stack<String> ops = new Stack<>(); // Operator stack
    
    public rpn(String input) {

        if(input.length() > 0) {
            infixToPostfix(input);
        }
        else {
            System.out.println("Error, empty input");
        }
    }
    
    // Use Shunting yard algorithm of E. Dijkstra
    private void infixToPostfix(String input) {
                
        if(input.contains(" "))
        {
            String[] parts = input.split(" ");
            for (String part : parts)
            {
                if (isNumeric(part))
                {
                    output.add(part);
                }
                else if (isOperator(part))
                {
                    if (rpn.opsMap.containsKey(part))
                    {
                        if (ops.empty())
                        {
                            ops.push(part);
                        }
                        else
                        {
                            while (!ops.empty() && (rpn.opsMap.get(part) < (double)rpn.opsMap.get(ops.peek()) ||
                                rpn.opsMap.get(part) == (double)rpn.opsMap.get(ops.peek())))
                            {
                                output.add(ops.pop());
                            }
                            ops.push(part);
                        }
                    } else {
                        System.out.println("Error, unsupported operator.");
                    }
                }
            }
            while(!ops.empty())
            {
                output.add(ops.pop());
            }
        } else
        {
            System.out.println("Error, string needs to be separated by spaces.");
        }
    }
    
    private boolean isNumeric(String str) {
        return str.matches("^(?:(?:\\-{1})?\\d+(?:\\.{1}\\d+)?)$");
    }
    
    private boolean isOperator(String str) {
        return str.matches("^[\\+\\-\\*\\/]$");
    }
    
    public static Map<String, Integer> opsMap;
    static {
        opsMap = new HashMap<>();
        opsMap.put("/", 2);
        opsMap.put("*", 2);
        opsMap.put("-", 1);
        opsMap.put("+", 1);
    }
    
    public String getOutput()
    {
        if(!output.isEmpty())
        {
            return String.join(" ", output);
        }
        return "Error output length: 0";
    }
    
    public String solve()
    {
        Stack<Double> stack = new Stack<>();
        Double answer;
        String retval;
        
        if(!output.isEmpty())
        {
            for (String part : output)
            {
                if (isNumeric(part))
                {
                    stack.push(Double.parseDouble(part));
                }
                else if (isOperator(part))
                {
                    double x, y;
                    
                    try {
                        y = stack.pop();
                    } catch (Exception e) {
                        return("error");
                    }
                    
                    try {
                        x = stack.pop();
                    } catch (Exception e) {
                        return("error");
                    }
                    
                    switch (part) {
                        case "+":{
                            try {
                                stack.push(x+y);
                            } catch (ArithmeticException e) {
                                return("error");
                            }
                            break;
                        }
                        case "-":{
                            try {
                                stack.push(x-y);
                            } catch (ArithmeticException e) {
                                return("error");
                            }
                            break;
                        }
                        case "*":{
                            try {
                                stack.push(x*y);
                            } catch (ArithmeticException e) {
                                return("error");
                            }
                            break;
                        }
                        case "/":{
                            try {
                                stack.push(x/y);
                            } catch (ArithmeticException e) {
                                return("error");
                            }
                            break;
                        }
                        default:
                            break;
                    }
                }
            }
        }
        answer = stack.pop();
        
        if(answer % Math.floor(answer) == 0.0) {
        	// return an integer
        	retval = String.valueOf(Math.round(answer));
        }
        else {
        	// return a rounded floating point number
        	retval = String.valueOf(Math.round(answer * 100.0) / 100.0);
        }
        return(retval);
    }
}
