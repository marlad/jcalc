/*******************************************************************************
 * Copyright (C) 2020, M. Klein
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

package nl.m9n.jcalc;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 */
public class Controller implements Initializable {

    private final ArrayList<String> input = new ArrayList<>();
            
    @FXML
    private TextArea display_result;
    @FXML
    private Button button_0;
    @FXML
    private Button button_1;
    @FXML
    private Button button_2;
    @FXML
    private Button button_3;
    @FXML
    private Button button_4;
    @FXML
    private Button button_5;
    @FXML
    private Button button_6;
    @FXML
    private Button button_7;
    @FXML
    private Button button_8;
    @FXML
    private Button button_9;
    @FXML
    private Button button_decimal_seperator;
    @FXML
    private Button button_add;
    @FXML
    private Button button_subtract;
    @FXML
    private Button button_multiply;
    @FXML
    private Button button_divide;
    @FXML
    private Button button_calculate;
    @FXML
    private Button button_all_clear;
    @FXML
    private Button button_clear;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void input(ActionEvent event) {

        if(event.getSource() == button_0) {
            validate("0");
        }
        if(event.getSource() == button_1) {
            validate("1");
        }
        if(event.getSource() == button_2) {
            validate("2");
        }
        if(event.getSource() == button_3) {
            validate("3");
        }
        if(event.getSource() == button_4) {
            validate("4");
        }
        if(event.getSource() == button_5) {
            validate("5");
        }
        if(event.getSource() == button_6) {
            validate("6");
        }
        if(event.getSource() == button_7) {
            validate("7");
        }
        if(event.getSource() == button_8) {
            validate("8");
        }
        if(event.getSource() == button_9) {
            validate("9");
        }
        if(event.getSource() == button_decimal_seperator) {
        	validate(".");
        }
        if(event.getSource() == button_add) {
            validate("+");
        }
        if(event.getSource() == button_subtract) {
            validate("-");
        }
        if(event.getSource() == button_multiply) {
            validate("*");
        }
        if(event.getSource() == button_divide) {
            validate("/");
        }
        // Clear all
        if(event.getSource() == button_all_clear) {
            input.clear();
        }
        // Only clear last element
        if(event.getSource() == button_clear) {
            if(input.size() > 0) {
                int last_index = input.size() - 1;
                input.remove(last_index);
            }
        }
        // if none of the above, we're probably done,
        // join array into a string, with values seperated
        // by a space character (for easier splitting).
        display_result.setText(String.join(" ", input));
    }

    // Test if input is a numeric
    private boolean isNumeric(String str) {
        return str.matches("^(?:(?:\\-{1})?\\d+(?:\\.{1}\\d+)?)$");
    }
    
    // Test if input is to become a floating point number, i.e. ending with a dot "."
    private boolean becomesFloatingPoint(String str) {
    	return str.matches("^(?:(?:\\-{1})?\\d+(?:\\.{1}))$");
    }
    
    // Test if input is a floating point number
    private boolean isFloatingPoint(String str) {
    	return str.matches("^(?:(?:\\-{1})?\\d+(?:\\.{1}\\d+))$");
    }
    
    private void validate(String new_value) {
        if(input.size() > 0) {
            int last_index = input.size() - 1;
            String last_value = (String) input.get(last_index);
            if(new_value.equals(".")) {
            	if(isNumeric(last_value) && !isFloatingPoint(last_value)) {
            		String new_number = String.join("", last_value, new_value);
            		input.set(last_index, new_number);
            	}
            }
            else if((isNumeric(last_value) || becomesFloatingPoint(last_value)) && isNumeric(new_value)) {
            	String new_number = String.join("", last_value, new_value);
            	input.set(last_index, new_number);
            } else if(!becomesFloatingPoint(last_value)){
            	input.add(new_value);
            }
        } else if(isNumeric(new_value)) {
            input.add(new_value);
        }
    }

    // Action for button_calculate
    @FXML
    private void calculate(ActionEvent event) {
        String txt = String.join(" ", input);
        rpn t = new rpn(txt);
        display_result.setText(txt + " = " + t.solve());
        input.clear();
    }
}
